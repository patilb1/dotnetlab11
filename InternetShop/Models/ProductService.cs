using System;
using System.Collections.Generic;
using System.Linq;
using InternetShop.Data;

namespace InternetShop.Models {
  public class ProductService : IProductService {
    public ProductService(InternetShopDbContext db) {
      this.db = db;
    }

    public IEnumerable<Product> AvailableProductsByQuantity() {
      return from p in db.Products
        where p.Quantity > 0
        orderby p.Quantity descending
        select p;
    }

    public IEnumerable<Product> AvailableProductsByPrice() {
      return from p in db.Products
        orderby p.Price
        select p;
    }

    public IEnumerable<Product> ProductsOnSale() {
      return from p in db.Products
        where p.OnSale == true
        orderby p.Price
        select p;
    }

    public InternetShopDbContext Db { get => db; }

    private readonly InternetShopDbContext db;
  }
}
